"""Defines the installations instructions.
"""

from setuptools import find_packages, setup

setup(
    name="puckpy",
    version="0.0.0",
    url="https://gitlab.com/l3robot/hockey-preds",
    author="Louis-Émile Robitaille",
    author_email="emile.courriel@gmail.com",
    description="A collection of hockey prediction code",
    packages=find_packages(),
)
